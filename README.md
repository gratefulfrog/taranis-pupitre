# Taranis Pupitre

Making a Taranis X9D support tray.

This support tray can be jig sawed or preferably laser cut from the DXF file in the Release directory.

I used 2 layers of 3mm plywood glued together to get better strength.

I suggest using m4 eye bolts, split rings and carabiners to attach the straps.

Straps can be made from 25mm webbing, D-rings, and small carabiners. Alternatively, premade straps are available in hobby shops.

A U-joint can be added to the arc that rests against the tummy, if needed ;-)

Good Flying!

![Taranis Support Tray](https://gitlab.com/gratefulfrog/taranis-pupitre/-/blob/main/Images/IMG_20210922_170136.jpg "Pupitre")


